clear;
close all;
% Plots camera trajectory vs ship/beacon 

% Beacon data
row_aft = [0,-1.25059]; % [m]  Positive aft
row_aft = [0,-1.27]; % [m]  Positive aft
row_port_lower = [0.9144, 0.3048, -0.3048, -0.9144]; % [m]  Positive port
row_port_upper = [0.9144, 0.3048, -0.3048, -0.9144]; % [m]
row_down = [0,-1.137]; % [m]  Positive down
% Lower row of beacons
beacon=zeros(8,3);
beacon(1,:)=[row_aft(1) row_port_lower(1) row_down(1)];
beacon(2,:)=[row_aft(1) row_port_lower(2) row_down(1)];
beacon(3,:)=[row_aft(1) row_port_lower(3) row_down(1)];
beacon(4,:)=[row_aft(1) row_port_lower(4) row_down(1)];
beacon(5,:)=[row_aft(2) row_port_upper(1) row_down(2)];
beacon(6,:)=[row_aft(2) row_port_upper(2) row_down(2)];
beacon(7,:)=[row_aft(2) row_port_upper(3) row_down(2)];
beacon(8,:)=[row_aft(2) row_port_upper(4) row_down(2)];

% Simulated Ship Deck
deck=zeros(6,3);
deck(1,:)=[0 -1.5 0];
deck(2,:)=[-3 -1.5 0];
deck(3,:)=[-4.5 0 0];
deck(4,:)=[-3 +1.5 0];
deck(5,:)=[0 +1.5 0];
deck(6,:)=[0 -1.5 0];

% bcs axes
bcs_origin=[0 0 0];
bcs_length=0.3048; % [m]
bcs_x=[bcs_length 0 0];
bcs_y=[0 bcs_length 0];
bcs_z=[0 0 bcs_length];
bcs_plot_x=[bcs_origin; bcs_length 0 0];
bcs_plot_y=[bcs_origin; 0 bcs_length 0];
bcs_plot_z=[bcs_origin; 0 0 bcs_length];

% aicraft axes;  traditional aircraft axes are x forward (cw roll+), y
% starboard (positive pitch +), z down (cw+)
acs_origin=[0 0 0];
acs_length=0.1525; % [m]
acs_x=[acs_length 0 0];
acs_y=[0 acs_length 0];
acs_z=[0 0 acs_length];
acs_plot_x=[acs_origin; acs_length 0 0];
acs_plot_y=[acs_origin; 0 acs_length 0];
acs_plot_z=[acs_origin; 0 0 acs_length];

% camera axes are (in image coordinates) x right, y down, z away from lens
ccs_origin=[0 0 0];
ccs_length=0.1525; % [m]
ccs_x=[ccs_length 0 0];
ccs_y=[0 ccs_length 0];
ccs_z=[0 0 ccs_length];

% probe axes are x aft, y starboard, z up
pcs_origin=[0 0 0];
pcs_length=0.1525; % [m]
pcs_x=[pcs_length 0 0];
pcs_y=[0 pcs_length 0];
pcs_z=[0 0 pcs_length];

% Quaternion notation.  q_xy rotates from coordinate system x to coordinate
% system y

% Conversion quaternions between camera and aircraft coordinates.  Camera
% is rolled so that camera_x=aircraft_z, camera_y=-aircraft_y,
% camera_z=aircraft_x
R_ca=[  0  0  1;
        0 -1  0;
        1  0  0];
q_ca = quaternion.rotationmatrix(R_ca); % Rotates camera coordinate to aircraft
q_ac = inverse(q_ca); % Rotates aircraft coordinates to camera

% Conversion between between probe and aircraft coordinates.  Probe
% convention is 
% probe_x=-aircraft_x, probe_y=aircraft_y, probe_z=-aircraft_z
R_ap=[ -1  0  0;
        0  1  0;
        0  0 -1];
q_ap = quaternion.rotationmatrix(R_ap); % Rotates aircraft coordinate to probe
q_pa = inverse(q_ap); % Rotates probe coordinates to aircraft

filename = 'rtvecs.csv';

[data,header,~] = xlsread(filename);

time=data(:,2);

tx=data(:,3);
ty=data(:,4);
tz=data(:,5);
rx=data(:,6);
ry=data(:,7);
rz=data(:,8);

% Compose tvec and rvec form the filtered data
rvec=[rx ry rz];
tvec=[tx ty tz];

% Here be dragons.  OpenCV use the rod=axis*theta convention for rotation
% vectors.
theta=vecnorm(rvec,2,2);
unitrvec=rvec./theta;

q_bc=quaternion.angleaxis(theta,unitrvec);   % Rotational transform from camera coords to beacon coords
q_cb=inverse(q_bc);                  % Rotational transform from beacon coords to camera coords

% tvec and rvec describe the beacon's origin and orientation in camera
% coordinates.
% Camera position relative to beacon coordinates is -tvec (relative position beacon->camera is - relative position
% camera->beacon, multiplied by q_cb
cpos_bcs=RotateVector(q_cb,-tvec);

% For now we assume aircraft position = camera position
apos_bcs=cpos_bcs;

% Here's a good place to filter the data.  For now, just rejecting data
% with unreasonable locations
%apos_bcs(apos_bcs(:,3)>0,1)=NaN;
%apos_bcs(apos_bcs(:,3)>0,2)=NaN;
%apos_bcs(apos_bcs(:,3)>0,3)=NaN;

% Aircraft coordinates are then just the composition of q_ac and q_cb 
q_ab=q_cb*q_ac;
q_ba=inverse(q_ab);

% Probe transform from probe to beacon coordinates can be built from q_ab
q_pb=q_ab*q_pa;
q_bp=inverse(q_pb);

% Rotate camera axes into bcs
ca_x_bcs=RotateVector(q_cb,ccs_x)';
ca_y_bcs=RotateVector(q_cb,ccs_y)';
ca_z_bcs=RotateVector(q_cb,ccs_z)';

% Rotate aircraft axes into bcs
aa_x_bcs=RotateVector(q_ab,acs_x)';
aa_y_bcs=RotateVector(q_ab,acs_y)';
aa_z_bcs=RotateVector(q_ab,acs_z)';

% Rotate probe axes into bcs
pa_x_bcs=RotateVector(q_pb,pcs_x)';
pa_z_bcs=RotateVector(q_pb,pcs_y)';
pa_y_bcs=RotateVector(q_pb,pcs_z)';

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
%plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
chx=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),ca_x_bcs(:,1),ca_x_bcs(:,2),ca_x_bcs(:,3),0,'Color',[1 0 0]);
chy=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),ca_y_bcs(:,1),ca_y_bcs(:,2),ca_y_bcs(:,3),0,'Color',[0 1 0]);
chz=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),ca_z_bcs(:,1),ca_z_bcs(:,2),ca_z_bcs(:,3),0,'Color',[0 0 1]);
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
view([-40 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
title('Camera coordinate system');

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
%plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
ahx=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),aa_x_bcs(:,1),aa_x_bcs(:,2),aa_x_bcs(:,3),0,'Color',[1 0 0]);
ahy=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),aa_y_bcs(:,1),aa_y_bcs(:,2),aa_y_bcs(:,3),0,'Color',[0 1 0]);
ahz=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),aa_z_bcs(:,1),aa_z_bcs(:,2),aa_z_bcs(:,3),0,'Color',[0 0 1]);
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
view([-40 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
title('Aircraft coordinate system');

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
%plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
phx=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),pa_x_bcs(:,1),pa_x_bcs(:,2),pa_x_bcs(:,3),0,'Color',[1 0 0]);
phy=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),pa_y_bcs(:,1),pa_y_bcs(:,2),pa_y_bcs(:,3),0,'Color',[0 1 0]);
phz=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),pa_z_bcs(:,1),pa_z_bcs(:,2),pa_z_bcs(:,3),0,'Color',[0 0 1]);
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
view([-40 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
title('Probe coordinate system');

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
%plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
plot3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),'r-')
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
view([-40 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
title('Trajectory');
