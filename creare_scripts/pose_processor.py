#!/usr/bin/env python

import cv2
import imutils
import numpy as np
import subprocess
import sys
import time
import glob
import re
from datetime import datetime
import argparse

from helper_functions import CentroidFinder, NoiseFilter, PnPSolver
from helper_functions import rectify, show_image

def status(length, percent):
  sys.stdout.write('\x1B[2K') # Erase entire current line
  sys.stdout.write('\x1B[0E') # Move to the beginning of the current line
  progress = "Progress: ["
  for i in range(0, length):
    if i < length * percent:
      progress += '='
    else:
      progress += ' '
  progress += "] " + str(round(percent * 100.0, 2)) + "%"
  sys.stdout.write(progress)
  sys.stdout.flush()

if __name__ == "__main__":

  # Set global variables
  filename = 'pose.csv'
  rtvecsfilename = 'rtvecs.csv'
  argsfilename = 'args.txt'
  poses = []
  rtvecs = []
  t_start = None
  initial_position=np.array([3.0,0,-0.30])
  last_position=initial_position
  last_ypr=np.array([0,0,180.0]) # Initialized with drone pointing at target
  frame_delta=1 # frames since last good fix.  Starts at 1 since we assume our initial position fix was good
  prev_frame_good=0 # was the last frame good?

  # New command line parser
  parser=argparse.ArgumentParser(description='Pose Processor')
  parser.add_argument('--rotate','-r',dest='rotate',action='store_true',help='rotate the image (default)')
  parser.add_argument('--no-rotate','-nr',dest='rotate',action='store_false',help='do not rotate the image')
  parser.add_argument('-i',dest='showImages',action='store_true',help='show images during processing (default)'    )
  parser.add_argument('-q',dest='showImages',action='store_false',help='quiet operation (no images)')
  parser.add_argument('--angle',type=float,default=15.0,help='allowable angular difference between images (degrees), default is 15.0')
  parser.add_argument('--cone',type=float,default=45.0,help='allowable cone angle for valid trajectories (degrees), default is 45.0')
  parser.add_argument('--shift',type=float,default=0.6,help='allowable linear difference between images (m), default is 0.6')
  parser.add_argument('--takeoff',type=float,default=15.0,help='time of takeoff (s), default is 15.0')
  parser.add_argument('--landing',type=float,default=210.0,help='time of landing (s), default is 210.0')
  parser.add_argument('--sunmask','-s',dest='sun_mask',action='store_true',help='mask the sun (left 40%% of image)')
  parser.add_argument('--no-sunmask','-ns',dest='sun_mask',action='store_false',help='do not mask the sun (default)')
  parser.add_argument('--debug','-d',dest='debug',action='store_true',help='debug messages')
  parser.add_argument('--debug-images','-D',dest='debugImages',action='store_true',help='show debug images')
    
  parser.set_defaults(sun_mask=False,rotate=True,showImages=True,debug=False)
  args = parser.parse_args()
  
  # Write headers to data
  poses.append('timestring,elapsed_time[s.ss],x[m],y[m],z[m],yaw[deg],pitch[deg],roll[deg],rx[deg],ry[deg],rz[deg]\n')
  rtvecs.append('timestring,elapsed_time[s.ss],tvec_x[m],tvec_y[m],tvec_z[m],rvec_x[rad],rvec_y[rad],rvec_z[rad]\n');
    
  # Manually setting camera data
  # Camera and distortions matrices, from BJD.  These are for the *non* rotated image.
  # For original calibrated image
  mtx = np.array([[1.07370451e+03, 0.00000000e+00, 1.04014231e+03],
       [0.00000000e+00, 1.07370451e+03, 7.78072511e+02],
       [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])
  # For half-scaled image
  mtx = 0.5*mtx

  dist = np.array([-3.19236204e-01,  1.04641507e-01,  2.49489004e-04,  4.89408842e-04,
       -1.55055271e-02])

  # Create processing objects
  cfinder = CentroidFinder(args.debugImages,args.debug)
  nfilter = NoiseFilter(args.debugImages,args.debug,args.rotate)
  psolver = PnPSolver(mtx, dist,args.debugImages,args.debug,args.rotate)
  
  images = glob.glob('img*.png')

  # If we're masking the sun, make the sun mask
  # Read the first image to get the size
  img_tmp = cv2.imread(images[0],cv2.IMREAD_GRAYSCALE)
  sun_line = int(0.4*img_tmp.shape[1])
  print(sun_line)
  sun_mask = np.full_like(img_tmp, 255, dtype='uint8')
  sun_mask[:,:sun_line]=0
  
  f=open(filename,'w')
  f2=open(rtvecsfilename,'w')
  f3=open(argsfilename,'w')
  f3.write(str(args))

  for fname in images:
    
    timeinfo=re.search(r'(\d+)-(\d+)-(\d+)_(\d+)-(\d+)-(\d+.\d+)',fname)
    timestring="%s:%s:%s"%(timeinfo.groups()[3:None])
    isotimestring="%s-%s-%s %s:%s:%s"%(timeinfo.groups())
    timeobj=datetime.fromisoformat(isotimestring)
       
    if t_start is None:
      t_start = timeobj;
      t = 0
    else:
      t = (timeobj-t_start).total_seconds()

    img = cv2.imread(fname,cv2.IMREAD_GRAYSCALE)
    
    if prev_frame_good:
      img=cv2.bitwise_and(img,img,mask = img_mask)  
    
    if args.sun_mask:
      img=cv2.bitwise_and(img,img,mask = sun_mask);
    
    # Find initial centroids
    centroids, img_cent = cfinder.get_centroids(img)
    
    if args.debugImages:
      show_image('initial centroids', img_cent, flag = args.showImages)
    
    # Process for noise
    centroids, img_filt = nfilter.filter_noise(img, centroids)
    if args.debugImages:
      show_image('filtered centroids', img_filt, flag = args.showImages)
    
    # Solve for pose
    if t>=args.takeoff and t<=args.landing:
      tvecs, rvecs, position, yawpitchroll, orientation, img_solv, img_mask = psolver.solve_pnp(img, centroids)
      show_image('Beacons', img_solv, duration = 1, flag = args.showImages)
      #cv2.imwrite('debug.png',img_solv)
    else:
      # Don't spin the wheels before takeoff or after landing
      rvecs = (None,None,None)
      position = (None,None,None)
      yawpitchroll = (None,None,None)
      orientation = (None,None,None)
      show_image('Beacons', img_filt, duration = 1, flag = args.showImages)
      
      
    print('%s %0.3f'%(fname,t),end='')
        
    # Save pose to list.  Filter out unsolved and invalid poses
    if (not position[0] is None):
      #import pdb; pdb.set_trace()
      x,y,z = position
      yaw,pitch,roll = yawpitchroll
      rz,ry,rx = orientation
        
      # Calculate deltas since last 
      delta_r=position-last_position
      shift_r=np.linalg.norm(delta_r)
            
      # quicker to estimate orientation from max euler angle change
      last_yaw,last_pitch,last_roll=last_ypr
      delta_yaw=abs(yaw-last_yaw)
      delta_pitch=abs(pitch-last_pitch)
      delta_roll=abs(roll-last_roll)
      shift_theta=max(delta_yaw,delta_pitch,delta_roll)
      
      print('(%0.2f,%0.2f)'%(shift_r,shift_theta),end='')
      
      # Calculate the cone angle
      cone_angle=(180.0/np.pi)*np.arctan2(np.sqrt(y**2+z**2),x)
      
      # Assume point is good until we know otherwide
      good_fix=1
      
      # Test point against various success criteria
      
      # Is point more than arg.shift*frame_delta away from the last good point?
      # (we make this value increment to avoid never getting a good fix again)
      if shift_r>(frame_delta*args.shift):
        good_fix=0
        frame_delta+=1
        print('r',end='')
      
      # Is the angle more than arg.thata*frame_delta away from the last good point?
      if shift_theta>(frame_delta*args.angle):
        good_fix=0
        frame_delta+=1
        print('a',end='')
      
      # Is the point below the array base (remember that up is -z)
      if z>0:
        good_fix=0
        frame_delta+=1
        print('l'%z,end='')
        
      # Is the cone angle too large?
      if cone_angle>args.cone:
        good_fix=0
        frame_delta+=1
        print('c',end='')

      # Are we before takeoff?
      if t<args.takeoff:
        good_fix=0
        frame_delta=1 # We don't want to accumulate potential error before takeoff
        print('t',end='')
      
      # Are we after landing?
      if t>args.landing:
        good_fix=0
        frame_delta=1 
        print('x',end='')
            
      if good_fix:
        poses.append('%s,%0.3f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f\n' %(timestring,t,x,y,z,yaw,pitch,roll,rx,ry,rz))
        rtvecs.append('%s,%0.3f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f\n' %(timestring,t,tvecs[0],tvecs[1],tvecs[2],rvecs[0],rvecs[1],rvecs[2]))
        last_position=position
        last_ypr=yawpitchroll
        frame_delta=1
        prev_frame_good=1
        print('+');
      else:
        prev_frame_good=0
        # reset the mask
        img_mask=np.full_like(img, 255, dtype='uint8')
        print('-');
    else: 
      # We've got a lack of any fix.  Increment frame_delta and move on
      frame_delta+=1
      # reset the mask
      img_mask=np.full_like(img, 255, dtype='uint8')
      print('n-');

    # We want to cap frame_delta so it doesn't get ridiculously large, but need to let this grow around launch or we miss the good first fix
    if frame_delta>8 and t>args.takeoff:
      frame_delta=8

    # In the event of an error, we don't want to lose too much information. Save to file every so many lines.
    if len(poses) > 10:
      # Write to file
      for p in poses:
        f.write(p)
      for rp in rtvecs:
        f2.write(rp)
      poses = []
      rtvecs = []

  # Write the final few poses to file
  for p in poses:
    f.write(p)
  for rp in rtvecs:
    f2.write(rp)
  status(40, 1)

  f.close();
  f2.close();