#!/usr/bin/env python

import cv2
import imutils
import numpy as np
import sys
import time
import glob
import re
from datetime import datetime
import argparse

from helper_functions import rectify, show_image

if __name__ == "__main__":

  # Set global variables
  t_start = None

  images = glob.glob('img*.png')

  for fname in images:
    
    timeinfo=re.search(r'(\d+)-(\d+)-(\d+)_(\d+)-(\d+)-(\d+.\d+)',fname)
    timestring="%s:%s:%s"%(timeinfo.groups()[3:None])
    isotimestring="%s-%s-%s %s:%s:%s"%(timeinfo.groups())
    timeobj=datetime.fromisoformat(isotimestring)
       
    if t_start is None:
      t_start = timeobj;
      t = 0
    else:
      t = (timeobj-t_start).total_seconds()

    img = cv2.imread(fname,cv2.IMREAD_GRAYSCALE)
    print('%s %0.3f'%(fname,t))
    show_image('image', img, flag = 1)

    