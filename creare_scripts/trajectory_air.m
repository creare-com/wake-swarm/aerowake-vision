clear;
close all;

% Plots camera trajectory vs ship/beacon 


%% Load defaults if they exist
defaults=matfile('defaults.mat');

% Process defaults
if isprop(defaults,'sweep_distance')  % Nominal tether deployment
   sweep_distance=defaults.sweep_distance; 
else
   sweep_distance=5; 
end

if isprop(defaults,'sweep_delta')  % how close should we be to the ideal arc
   sweep_delta=defaults.sweep_delta; 
else
   sweep_delta=0.5; 
end

if isprop(defaults,'vx_size')
   vx_size=defaults.vx_size; 
else
   vx_size=0.2; 
end

if isprop(defaults,'truck_start')
   truck_start=defaults.truck_start; 
else
   truck_start=100; 
end

if isprop(defaults,'wind_speed')
   wind_speed=defaults.wind_speed; 
else
   wind_speed=4.5; 
end


%% Initial camera data setup
% Beacon data
%row_aft = [0,-1.25059]; % [m]  Positive aft
row_aft = [0,-1.27]; % [m]  Positive aft
row_port_lower = [0.9144, 0.3048, -0.3048, -0.9144]; % [m]  Positive port
row_port_upper = [0.9144, 0.3048, -0.3048, -0.9144]; % [m]
row_down = [0,-1.137]; % [m]  Positive down
% Lower row of beacons
beacon=zeros(8,3);
beacon(1,:)=[row_aft(1) row_port_lower(1) row_down(1)];
beacon(2,:)=[row_aft(1) row_port_lower(2) row_down(1)];
beacon(3,:)=[row_aft(1) row_port_lower(3) row_down(1)];
beacon(4,:)=[row_aft(1) row_port_lower(4) row_down(1)];
beacon(5,:)=[row_aft(2) row_port_upper(1) row_down(2)];
beacon(6,:)=[row_aft(2) row_port_upper(2) row_down(2)];
beacon(7,:)=[row_aft(2) row_port_upper(3) row_down(2)];
beacon(8,:)=[row_aft(2) row_port_upper(4) row_down(2)];

% Simulated Ship Deck
deck=zeros(6,3);
deck(1,:)=[0 -1.5 0];
deck(2,:)=[-3 -1.5 0];
deck(3,:)=[-4.5 0 0];
deck(4,:)=[-3 +1.5 0];
deck(5,:)=[0 +1.5 0];
deck(6,:)=[0 -1.5 0];

% bcs axes; x aft, y port, z down
bcs_origin=[0 0 0];
bcs_length=0.3048; % [m]
bcs_x=[bcs_length 0 0];
bcs_y=[0 bcs_length 0];
bcs_z=[0 0 bcs_length];
bcs_plot_x=[bcs_origin; bcs_length 0 0];
bcs_plot_y=[bcs_origin; 0 bcs_length 0];
bcs_plot_z=[bcs_origin; 0 0 bcs_length];

% Set the tether distance
pivot_origin=[0.6223 0 0];
tether_offset = 1.20; % distance from pivot point to launch point
sweep_radius=sweep_distance+tether_offset;

% aicraft axes;  traditional aircraft axes are x forward (cw roll+), y
% starboard (positive pitch +), z down (cw+)
acs_origin=[0 0 0];
acs_length=0.1525; % [m]
acs_x=[acs_length 0 0];
acs_y=[0 acs_length 0];
acs_z=[0 0 acs_length];

% camera axes are (in image coordinates) x right, y down, z away from lens
ccs_origin=[0 0 0];
ccs_length=0.1525; % [m]
ccs_x=[ccs_length 0 0];
ccs_y=[0 ccs_length 0];
ccs_z=[0 0 ccs_length];

% probe axes are x aft, y starboard, z up
pcs_origin=[0 0 0];
pcs_length=0.1525; % [m]
pcs_x=[pcs_length 0 0];
pcs_y=[0 pcs_length 0];
pcs_z=[0 0 pcs_length];

% Offsets for the probe and aircraft center locations, in *camera* coordinates
probe_offset=[0.0 0.09525 0.4953]; % [m]
aircraft_offset=[0.0 0.0 -0.5]; % [m]

% Quaternion notation.  q_xy rotates from coordinate system x to coordinate
% system y

% Conversion quaternions between camera and aircraft coordinates.  Camera
% is rolled so that camera_x=aircraft_z, camera_y=-aircraft_y,
% camera_z=aircraft_x
R_ca=[  0  0  1;
        0 -1  0;
        1  0  0];
q_ca = quaternion.rotationmatrix(R_ca); % Rotates camera coordinate to aircraft
q_ac = inverse(q_ca); % Rotates aircraft coordinates to camera

% Conversion between between probe and aircraft coordinates.  Probe
% convention is 
% probe_x=-aircraft_x, probe_y=aircraft_y, probe_z=-aircraft_z
R_ap=[ -1  0  0;
        0  1  0;
        0  0 -1];
q_ap = quaternion.rotationmatrix(R_ap); % Rotates aircraft coordinate to probe
q_pa = inverse(q_ap); % Rotates probe coordinates to aircraft

%% Initial wind data setup

addpath('C:\Program Files\Aeroprobe\AeroFlow Developer Kit - MATLAB\');

%% Load Camera Data

filename = 'rtvecs.csv';

[data,header,~] = xlsread(filename);

ctime=data(:,2);

tx=data(:,3);
ty=data(:,4);
tz=data(:,5);
rx=data(:,6);
ry=data(:,7);
rz=data(:,8);

% Compose tvec and rvec form the filtered data
rvec=[rx ry rz];
tvec=[tx ty tz];

%% Load raw air sensor data

% Check to see if we've done this already

if (isfile('wind_reduced.mat'))
    disp('loading previously reduced wind data');
    load('wind_reduced.mat');
else

    directory = dir('wp_*.csv');

    [data2,header2,~] = xlsread(directory.name);

    vtime=data2(:,2);

    load('pressure_bias.mat');

    dpressure=zeros(size(data2,1),12);
    dpressure(:,1)=data2(:,3)-pressure_bias(1);
    dpressure(:,2)=data2(:,5)-pressure_bias(2);
    dpressure(:,3)=data2(:,7)-pressure_bias(3);
    dpressure(:,4)=data2(:,9)-pressure_bias(4);
    dpressure(:,5)=data2(:,11)-pressure_bias(5);
    dpressure(:,6)=data2(:,13)-pressure_bias(6);
    dpressure(:,7)=data2(:,15)-pressure_bias(7);
    dpressure(:,8)=data2(:,17)-pressure_bias(8);
    dpressure(:,9)=data2(:,19)-pressure_bias(9);
    dpressure(:,10)=data2(:,21)-pressure_bias(10);
    dpressure(:,11)=data2(:,23)-pressure_bias(11);
    dpressure(:,12)=data2(:,25)-pressure_bias(12);
    apressure=data2(:,27);

    % Reduce the pressure data to airspeed

    % Building the data array for the Aerowake toolkit.  It wants columns for
    % all 12 pressure ports, pref, and tref. That means adding columns onto
    % dpressure:

    data_array=[dpressure apressure*27.68 data2(:,28)];

    % Configuring the Aerowake toolkit.

    calibs_to_use = {'C:\Projects\Aerowake\Probe Cal\1190507-1\1190507-1_M015.pcf';
                     'C:\Projects\Aerowake\Probe Cal\1190507-1\1190507-1_M072.pcf'};
    Temp_units = 'C';
    Press_units = 'inH2O';
    test_data_columns=1:14;

    [angles_calc,Ps_calc,Pt_calc,Mach_num_calc,Vel_Mag_calc,UVW_calc,...
        density_calc,Error_Flags,Temp_total,Temp_static,version] =...
        AeroFlow2_Reduction(calibs_to_use,data_array,test_data_columns,Temp_units,Press_units);
    disp('Reduction Complete for Data File, data saved to wind_reduced.mat')
    save('wind_reduced.mat','angles_calc','Ps_calc','Pt_calc','Mach_num_calc','Vel_Mag_calc','UVW_calc',...
        'density_calc','Error_Flags','Temp_total','Temp_static','version','dpressure','apressure','vtime');

end

%% Filter velocity data
UVW_calc(Error_Flags>200001,1)=NaN;
UVW_calc(Error_Flags>200001,2)=NaN;
UVW_calc(Error_Flags>200001,3)=NaN;
UVW_calc(Vel_Mag_calc<2,1)=NaN;  % Throw out low velocity stuff for now
UVW_calc(Vel_Mag_calc<2,2)=NaN;  
UVW_calc(Vel_Mag_calc<2,3)=NaN;  
UVW_calc(Vel_Mag_calc>20,1)=NaN;  % Throw out high velocity stuff for now
UVW_calc(Vel_Mag_calc>20,2)=NaN;  
UVW_calc(Vel_Mag_calc>20,3)=NaN;  


%% Camera system Coordinate system conversions

% Here be dragons.  OpenCV use the rod=axis*theta convention for rotation
% vectors.
theta=vecnorm(rvec,2,2);
unitrvec=rvec./theta;

q_bc=quaternion.angleaxis(theta,unitrvec);   % Rotational transform from camera coords to beacon coords
q_cb=inverse(q_bc);                  % Rotational transform from beacon coords to camera coords

% tvec and rvec describe the beacon's origin and orientation in camera
% coordinates.
% Camera position relative to beacon coordinates is -tvec (relative position beacon->camera is - relative positione
% camaera->beacon, multiplied by q_bc
cpos_bcs=RotateVector(q_cb,-tvec);

%% Transform the data between coordinate systems

% aircraft position = camera position + rotated aircraft offset vector
aircraft_offset_vector=RotateVector(q_cb,aircraft_offset)';
apos_bcs=cpos_bcs+aircraft_offset_vector;

% probe position = camera position + rotated aircraft offset vector
probe_offset_vector=RotateVector(q_cb,probe_offset)';
ppos_bcs=cpos_bcs+probe_offset_vector;

% Aircraft coordinates are then just the composition of q_ac and q_cb 
q_ab=q_cb*q_ac;
q_ba=inverse(q_ab);

% Probe transform from probe to beacon coordinates can be built from q_ab
% and q_pa
q_pb=q_ab*q_pa;
q_bp=inverse(q_pb);

% Rotate camera axes into bcs
ca_x_bcs=RotateVector(q_cb,ccs_x)';
ca_y_bcs=RotateVector(q_cb,ccs_y)';
ca_z_bcs=RotateVector(q_cb,ccs_z)';

% Rotate aircraft axes into bcs
aa_x_bcs=RotateVector(q_ab,acs_x)';
aa_y_bcs=RotateVector(q_ab,acs_y)';
aa_z_bcs=RotateVector(q_ab,acs_z)';

% Rotate probe axes into bcs
pa_x_bcs=RotateVector(q_pb,pcs_x)';
pa_z_bcs=RotateVector(q_pb,pcs_y)';
pa_y_bcs=RotateVector(q_pb,pcs_z)';

%% Kinematic estimation

% Calculate the difference between successive positions
ppos_bcs_deltar=diff(ppos_bcs);
deltat=diff(ctime);

ppos_bcs_v=ppos_bcs_deltar./deltat;

% Pad this to make it line up with the original position data
ppos_bcs_v=[ppos_bcs_v(1,:);ppos_bcs_v];

% This data is noisy, so do a moving mean of it to remove high frequency
% noise

ppos_bcs_v=movmean(ppos_bcs_v,13);


%% Airspeed sensor coordinate conversion

% The airspeed data is more frequent than the image data.  For each value
% of vtime, we need to find the index of the nearest value of ctime

[minValue,imatch] = min(abs(bsxfun(@minus,ctime, vtime')));
q_pb_closest=q_pb(imatch);
windvector_bcs=RotateVector(q_pb_closest,UVW_calc);
% Need the approximated position:
ppos_wind_bcs=ppos_bcs(imatch,:);
% And the estimated velocity:
ppos_wind_bcs_v=ppos_bcs_v(imatch,:);

% Now, we need to correct the wind vector to add the relative wind
% from the probe motion (probe perceived wind is opposite of the vector, so
% we're subtracted the negative vector, aka adding)
windvector_bcs=windvector_bcs+ppos_wind_bcs_v;

%% Select only those data points collected "on station"

% The probe location is at almost the exact same radial location from the
% tether central pivot as the tether connector on the drone, within noise,
% so we can filter on the probe position, ppos_bcs

% Offset ppos_bcs and take the radial component, which is essentially the
% real tether length
ppos_wind_bcs_offset=ppos_wind_bcs-pivot_origin;
[~,~,ppos_wind_radial]=cart2sph(ppos_wind_bcs_offset(:,1),ppos_wind_bcs_offset(:,2),ppos_wind_bcs_offset(:,3));

% We also want to to estimate the rate of change of this value to filter on
% it
ppos_wind_radial_delta=diff(ppos_wind_radial);
vdeltat=diff(vtime);
ppos_wind_radial_v=ppos_wind_radial_delta./vdeltat;

% Pad this to make it line up with the original position data
ppos_wind_radial_v=[ppos_wind_radial_v(1);ppos_wind_radial_v];

% This data is noisy, so do a moving mean of it to remove high frequency
% noise
ppos_wind_radial_v=movmean(ppos_wind_radial_v,5);

% Only select those data within the sweep shell
%on_station=ppos_wind_radial<(sweep_radius+sweep_delta) & ppos_wind_radial>(sweep_radius-sweep_delta);
on_station_sweep=ppos_wind_radial<(sweep_radius+sweep_delta) & ppos_wind_radial>(sweep_radius-sweep_delta) & ppos_wind_radial_v<1.0;

on_station_time=vtime>truck_start;

on_station=on_station_sweep&on_station_time;

ppos_wind_bcs_station=ppos_wind_bcs(on_station,:);
ppos_wind_bcs_v_station=ppos_wind_bcs_v(on_station,:);
windvector_bcs_station=windvector_bcs(on_station,:);

%% Voxel segmentation

% vx_size now set above

% Get bounds on the data
xmin=min(ppos_wind_bcs_station(:,1));
ymin=min(ppos_wind_bcs_station(:,2));
zmin=min(ppos_wind_bcs_station(:,3));
xmax=max(ppos_wind_bcs_station(:,1));
ymax=max(ppos_wind_bcs_station(:,2));
zmax=max(ppos_wind_bcs_station(:,3));

% Create voxel boundary limits
vx_xmin=floor(xmin)+floor((xmin-floor(xmin))/vx_size)*vx_size;
vx_xmax=floor(xmax)+ceil((xmax-floor(xmax))/vx_size)*vx_size;
vx_ymin=floor(ymin)+floor((ymin-floor(ymin))/vx_size)*vx_size;
vx_ymax=floor(ymax)+ceil((ymax-floor(ymax))/vx_size)*vx_size;
vx_zmin=floor(zmin)+floor((zmin-floor(zmin))/vx_size)*vx_size;
vx_zmax=floor(zmax)+ceil((zmax-floor(zmax))/vx_size)*vx_size;

% We want the data to bracket the points, so the min point will be
% vx_xmin+0.5*vx_size, the max point will be vx_xmax-0.5*vxsize
vx_xrange=[vx_xmin+vx_size/2:vx_size:vx_xmax-vx_size/2];
vx_yrange=[vx_ymin+vx_size/2:vx_size:vx_ymax-vx_size/2];
vx_zrange=[vx_zmin+vx_size/2:vx_size:vx_zmax-vx_size/2];

% Create the mesh grid
[X,Y,Z]=meshgrid(vx_xrange,vx_yrange,vx_zrange);

% It's easier for the processing if we flatten things
Xflat=reshape(X,[],1);
Yflat=reshape(Y,[],1);
Zflat=reshape(Z,[],1);

% Create storage for statistics
U=NaN*ones(size(Xflat));
V=NaN*ones(size(Xflat));
W=NaN*ones(size(Xflat));
Urms=NaN*ones(size(Xflat));
Vrms=NaN*ones(size(Xflat));
Wrms=NaN*ones(size(Xflat));
TKE=NaN*ones(size(Xflat));

% Create the voxel membership map
VXflat=zeros(size(ppos_wind_bcs_station(:,1)));

% Assign individual data points to each voxel, store in VXflat
for i=1:size(Xflat,1)
   xIdx=(ppos_wind_bcs_station(:,1)>=Xflat(i)-vx_size/2)&(ppos_wind_bcs_station(:,1)<=Xflat(i)+vx_size/2);
   yIdx=(ppos_wind_bcs_station(:,2)>=Yflat(i)-vx_size/2)&(ppos_wind_bcs_station(:,2)<=Yflat(i)+vx_size/2);
   zIdx=(ppos_wind_bcs_station(:,3)>=Zflat(i)-vx_size/2)&(ppos_wind_bcs_station(:,3)<=Zflat(i)+vx_size/2);
   Idx=xIdx&yIdx&zIdx;
   VXflat(Idx)=i;
end

% For each voxel, calculate stats
for i=1:size(Xflat,1)
    U(i)=mean(windvector_bcs_station(VXflat==i&~isnan(windvector_bcs_station(:,1)),1));
    V(i)=mean(windvector_bcs_station(VXflat==i&~isnan(windvector_bcs_station(:,2)),2));
    W(i)=mean(windvector_bcs_station(VXflat==i&~isnan(windvector_bcs_station(:,3)),3));
    % May want to scale these on freestream:
    Urms(i)=rms(windvector_bcs_station(VXflat==i&~isnan(windvector_bcs_station(:,1)),1));
    Vrms(i)=rms(windvector_bcs_station(VXflat==i&~isnan(windvector_bcs_station(:,2)),2));
    Wrms(i)=rms(windvector_bcs_station(VXflat==i&~isnan(windvector_bcs_station(:,3)),3));
    TKE(i)=0.5*(Urms(i)^2+Vrms(i)^2+Wrms(i)^2);
end

% %% Air velocity vector plotting
% f=figure;
% subplot(3,1,1)
% plot(vtime,UVW_calc(:,1));
% title('Air UVW relative to probe body')
% ylabel('u [m/s]')
% subplot(3,1,2)
% plot(vtime,UVW_calc(:,2));
% ylabel('v [m/s]')
% subplot(3,1,3)
% plot(vtime,UVW_calc(:,3));
% ylabel('w [m/s]')
% xlabel('time [s]')
% 
% f=figure;
% subplot(3,1,1)
% plot(vtime,windvector_bcs(:,1));
% title('Air UVW relative to trailer coordinates')
% ylabel('u [m/s]')
% subplot(3,1,2)
% plot(vtime,windvector_bcs(:,2));
% ylabel('v [m/s]')
% subplot(3,1,3)
% plot(vtime,windvector_bcs(:,3));
% ylabel('w [m/s]')
% xlabel('time [s]')


%% Trajectory plotting
close all;

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
%plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
plot3(ppos_bcs(:,1),ppos_bcs(:,2),ppos_bcs(:,3),'r-')
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Probe Tip Trajectory');
savefig('trajectory.fig');

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
%plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
plot3(ppos_wind_bcs_station(:,1),ppos_wind_bcs_station(:,2),ppos_wind_bcs_station(:,3),'r-')
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Probe Tip Trajectory (on station)');
savefig('trajectory-station.fig');

% f=figure;
% plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
% axis equal;
% hold on;
% plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
% plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
% plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
% %plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
% chx=quiver3(cpos_bcs(:,1),cpos_bcs(:,2),cpos_bcs(:,3),ca_x_bcs(:,1),ca_x_bcs(:,2),ca_x_bcs(:,3),0,'Color',[1 0 0]);
% chy=quiver3(cpos_bcs(:,1),cpos_bcs(:,2),cpos_bcs(:,3),ca_y_bcs(:,1),ca_y_bcs(:,2),ca_y_bcs(:,3),0,'Color',[0 1 0]);
% chz=quiver3(cpos_bcs(:,1),cpos_bcs(:,2),cpos_bcs(:,3),ca_z_bcs(:,1),ca_z_bcs(:,2),ca_z_bcs(:,3),0,'Color',[0 0 1]);
% f.CurrentAxes.ZDir='Reverse';
% f.CurrentAxes.YDir='Reverse';
% view([60 15]);
% xlabel('x [m]');
% ylabel('y [m]');
% zlabel('z [m]');
% title('Camera coordinate system');

% f=figure;
% plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
% axis equal;
% hold on;
% plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
% plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
% plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
% %plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
% ahx=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),aa_x_bcs(:,1),aa_x_bcs(:,2),aa_x_bcs(:,3),0,'Color',[1 0 0]);
% ahy=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),aa_y_bcs(:,1),aa_y_bcs(:,2),aa_y_bcs(:,3),0,'Color',[0 1 0]);
% ahz=quiver3(apos_bcs(:,1),apos_bcs(:,2),apos_bcs(:,3),aa_z_bcs(:,1),aa_z_bcs(:,2),aa_z_bcs(:,3),0,'Color',[0 0 1]);
% f.CurrentAxes.ZDir='Reverse';
% f.CurrentAxes.YDir='Reverse';
% view([60 15]);
% xlabel('x [m]');
% ylabel('y [m]');
% zlabel('z [m]');
% title('Aircraft coordinate system');
% 
% f=figure;
% plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
% axis equal;
% hold on;
% plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
% plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
% plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
% %plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
% phx=quiver3(ppos_bcs(:,1),ppos_bcs(:,2),ppos_bcs(:,3),pa_x_bcs(:,1),pa_x_bcs(:,2),pa_x_bcs(:,3),0,'Color',[1 0 0]);
% phy=quiver3(ppos_bcs(:,1),ppos_bcs(:,2),ppos_bcs(:,3),pa_y_bcs(:,1),pa_y_bcs(:,2),pa_y_bcs(:,3),0,'Color',[0 1 0]);
% phz=quiver3(ppos_bcs(:,1),ppos_bcs(:,2),ppos_bcs(:,3),pa_z_bcs(:,1),pa_z_bcs(:,2),pa_z_bcs(:,3),0,'Color',[0 0 1]);
% f.CurrentAxes.ZDir='Reverse';
% f.CurrentAxes.YDir='Reverse';
% view([60 15]);
% xlabel('x [m]');
% ylabel('y [m]');
% zlabel('z [m]');
% title('Probe coordinate system');

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
scale=0.2;
phx=quiver3(ppos_bcs(:,1),ppos_bcs(:,2),ppos_bcs(:,3),scale*ppos_bcs_v(:,1),scale*ppos_bcs_v(:,2),scale*ppos_bcs_v(:,3),0,'Color',[0 0 1]);
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Probe kinematic vectors');
savefig('kinematic.fig');


% %% Wind vector plotting
% f=figure;
% plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
% axis equal;
% hold on;
% plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
% plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
% plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
% %plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
% plot3(ppos_wind_bcs(:,1),ppos_wind_bcs(:,2),ppos_wind_bcs(:,3),'b-')
% phx=quiver3(ppos_wind_bcs(:,1),ppos_wind_bcs(:,2),ppos_wind_bcs(:,3),windvector_bcs(:,1),windvector_bcs(:,2),windvector_bcs(:,3),0);
% f.CurrentAxes.ZDir='Reverse';
% f.CurrentAxes.YDir='Reverse';
% view([60 15]);
% xlabel('x [m]');
% ylabel('y [m]');
% zlabel('z [m]');
% title('Measured Air Velocity');

% %% Wind vector plotting
% f=figure;
% plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
% axis equal;
% hold on;
% plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
% plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
% plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
% %plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
% plot3(ppos_wind_bcs(:,1),ppos_wind_bcs(:,2),ppos_wind_bcs(:,3),'b-')
% phx=quiver3(ppos_wind_bcs(:,1),ppos_wind_bcs(:,2),ppos_wind_bcs(:,3),windvector_bcs(:,1),zeros(size(windvector_bcs(:,2))),zeros(size(windvector_bcs(:,2))),0);
% f.CurrentAxes.ZDir='Reverse';
% f.CurrentAxes.YDir='Reverse';
% view([60 15]);
% xlabel('x [m]');
% ylabel('y [m]');
% zlabel('z [m]');
% title('Measured Air Velocity, Streamwise Component');

% %% Wind vector plotting
% f=figure;
% plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
% axis equal;
% hold on;
% plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
% plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
% plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
% %plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
% plot3(ppos_wind_bcs(:,1),ppos_wind_bcs(:,2),ppos_wind_bcs(:,3),'b-')
% phx=quiver3(ppos_wind_bcs(:,1),ppos_wind_bcs(:,2),ppos_wind_bcs(:,3),zeros(size(windvector_bcs(:,2))),windvector_bcs(:,2),zeros(size(windvector_bcs(:,2))),0);
% f.CurrentAxes.ZDir='Reverse';
% f.CurrentAxes.YDir='Reverse';
% view([60 15]);
% xlabel('x [m]');
% ylabel('y [m]');
% zlabel('z [m]');
% title('Measured Air Velocity, Crossstream Component');

% %% Wind vector plotting
% f=figure;
% plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
% axis equal;
% hold on;
% plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
% plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
% plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
% %plot3(bcs_plot_z(:,1),bcs_plot_z(:,2),bcs_plot_z(:,3),'b-');
% plot3(ppos_wind_bcs(:,1),ppos_wind_bcs(:,2),ppos_wind_bcs(:,3),'b-')
% phx=quiver3(ppos_wind_bcs(:,1),ppos_wind_bcs(:,2),ppos_wind_bcs(:,3),zeros(size(windvector_bcs(:,2))),zeros(size(windvector_bcs(:,2))),windvector_bcs(:,3),0);
% f.CurrentAxes.ZDir='Reverse';
% f.CurrentAxes.YDir='Reverse';
% view([60 15]);
% xlabel('x [m]');
% ylabel('y [m]');
% zlabel('z [m]');
% title('Measured Air Velocity, Vertical Component');

%% Wind voxel vector plotting
vscale=0.5;
f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
phx=quiver3(Xflat,Yflat,Zflat,U*vscale,V*vscale,W*vscale,0,'b-');
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Measured Air Vectors, Spatially averaged');
savefig('vectors.fig');


%% TKE voxel plotting
tke_colormap='jet';
tke_size=5;
min_tke=0;
% Round up to the nearest 5, or
max_tke=ceil(max(TKE) / tke_size) * tke_size;
% Just set it (useful for comparison between runs)
max_tke=50;

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
for i=1:size(TKE,1)
    if ~isnan(TKE(i))
        voxel([Xflat(i)-0.5*vx_size;Yflat(i)-0.5*vx_size;Zflat(i)-0.5*vx_size],[vx_size;vx_size;vx_size],vals2colormap(TKE(i),tke_colormap,[0;max_tke]),'0.75');
    end
end
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
caxis([0 max_tke]);
colorbar;
colormap(tke_colormap);
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Turbulent Kinetic Energy by Voxel [m^2/s^2]')
savefig('tke.fig');


%% U voxel plotting
U_colormap='jet';
U_size=0.5;
min_U=0;
% Round up to the nearest 5, or
max_U=ceil(max(U) / U_size) * U_size;
% Just set it (useful for comparison between runs)
max_U=10;

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
for i=1:size(U,1)
    if ~isnan(U(i))
        voxel([Xflat(i)-0.5*vx_size;Yflat(i)-0.5*vx_size;Zflat(i)-0.5*vx_size],[vx_size;vx_size;vx_size],vals2colormap(U(i),U_colormap,[0;max_U]),'0.75');
    end
end
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
caxis([0 max_U]);
colorbar;
colormap(U_colormap);
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Streamwise Velocity Component by Voxel [m/s]')
savefig('U.fig');

%% V voxel plotting
V_colormap='jet';
V_size=0.5;
min_V=0;
% Round up to the nearest 5, or
max_V=ceil(max(V) / V_size) * V_size;
% Just set it (useful for comparison between runs)
max_V=3;

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
for i=1:size(V,1)
    if ~isnan(V(i))
        voxel([Xflat(i)-0.5*vx_size;Yflat(i)-0.5*vx_size;Zflat(i)-0.5*vx_size],[vx_size;vx_size;vx_size],vals2colormap(V(i),V_colormap,[0-max_V;max_V]),'0.75');
    end
end
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
caxis([-max_V max_V]);
colorbar;
colormap(V_colormap);
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Lateral Velocity Component by Voxel [m/s]')
savefig('V.fig');

%% W voxel plotting
W_colormap='jet';
W_size=0.5;
min_W=0;
% Round up to the nearest 5, or
max_W=ceil(max(W) / W_size) * W_size;
% Just set it (useful for comparison between runs)
max_W=3;

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
for i=1:size(W,1)
    if ~isnan(W(i))
        voxel([Xflat(i)-0.5*vx_size;Yflat(i)-0.5*vx_size;Zflat(i)-0.5*vx_size],[vx_size;vx_size;vx_size],vals2colormap(W(i),W_colormap,[-max_W;max_W]),'0.75');
    end
end
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
caxis([-max_W max_W]);
colorbar;
colormap(W_colormap);
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Vertical Velocity Component by Voxel [m/s]')
savefig('W.fig');

%% U normalize voxel plotting
Un_colormap='jet';
Un_size=0.5;
min_Un=0;
max_Un=2;
Un=U/wind_speed;

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
for i=1:size(Un,1)
    if ~isnan(Un(i))
        voxel([Xflat(i)-0.5*vx_size;Yflat(i)-0.5*vx_size;Zflat(i)-0.5*vx_size],[vx_size;vx_size;vx_size],vals2colormap(Un(i),Un_colormap,[0;max_Un]),'0.75');
    end
end
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
caxis([min_Un max_Un]);
colorbar;
colormap(Un_colormap);
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Normalized Streamwise Velocity Component by Voxel')
savefig('Un.fig');

%% Urms voxel plotting
Urms_colormap='jet';
Urms_size=0.5;
min_Urms=0;
% Round up to the nearest 5, or
max_Urms=ceil(max(Urms) / Urms_size) * Urms_size;
% Just set it (useful for comparison between runs)
max_Urms=10;

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
for i=1:size(Urms,1)
    if ~isnan(Urms(i))
        voxel([Xflat(i)-0.5*vx_size;Yflat(i)-0.5*vx_size;Zflat(i)-0.5*vx_size],[vx_size;vx_size;vx_size],vals2colormap(Urms(i),Urms_colormap,[0;max_Urms]),'0.75');
    end
end
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
caxis([0 max_Urms]);
colorbar;
colormap(Urms_colormap);
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Streamwise Velocity rms by Voxel [m/s]')
savefig('Urms.fig');

%% Vrms voxel plotting
Vrms_colormap='jet';
Vrms_size=0.5;
min_Vrms=0;
% Round up to the nearest 5, or
max_Vrms=ceil(max(Vrms) / Vrms_size) * Vrms_size;
% Just set it (useful for comparison between runs)
max_Vrms=5;

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
for i=1:size(Vrms,1)
    if ~isnan(Vrms(i))
        voxel([Xflat(i)-0.5*vx_size;Yflat(i)-0.5*vx_size;Zflat(i)-0.5*vx_size],[vx_size;vx_size;vx_size],vals2colormap(Vrms(i),Vrms_colormap,[0;max_Vrms]),'0.75');
    end
end
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
caxis([0 max_Vrms]);
colorbar;
colormap(Vrms_colormap);
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Lateral Velocity rms by Voxel [m/s]')
savefig('Vrms.fig');

%% Wrms voxel plotting
Wrms_colormap='jet';
Wrms_size=0.5;
min_Wrms=0;
% Round up to the nearest 5, or
max_Wrms=ceil(max(Wrms) / Wrms_size) * Wrms_size;
% Just set it (useful for comparison between runs)
max_Wrms=5;

f=figure;
plot3(beacon(:,1),beacon(:,2),beacon(:,3),'bo');
axis equal;
hold on;
plot3(deck(:,1),deck(:,2),deck(:,3),'k-');
plot3(bcs_plot_x(:,1),bcs_plot_x(:,2),bcs_plot_x(:,3),'r-');
plot3(bcs_plot_y(:,1),bcs_plot_y(:,2),bcs_plot_y(:,3),'g-');
for i=1:size(Wrms,1)
    if ~isnan(Wrms(i))
        voxel([Xflat(i)-0.5*vx_size;Yflat(i)-0.5*vx_size;Zflat(i)-0.5*vx_size],[vx_size;vx_size;vx_size],vals2colormap(Wrms(i),Wrms_colormap,[0;max_Wrms]),'0.75');
    end
end
f.CurrentAxes.ZDir='Reverse';
f.CurrentAxes.YDir='Reverse';
caxis([0 max_Wrms]);
colorbar;
colormap(Wrms_colormap);
view([60 15]);
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
grid on;
title('Vertical Velocity rms by Voxel [m/s]')
savefig('Wrms.fig');