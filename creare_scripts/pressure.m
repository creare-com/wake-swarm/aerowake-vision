clear;
close all;

directory = dir('wp_*.csv');

[data,header,~] = xlsread(directory.name);

time=data(:,2);

pressure_bias=[mean(data(:,3));
               mean(data(:,5));
               mean(data(:,7));
               mean(data(:,9));
               mean(data(:,11));
               mean(data(:,13));
               mean(data(:,15));
               mean(data(:,17));
               mean(data(:,19));
               mean(data(:,21));
               mean(data(:,23));
               mean(data(:,25))];
           
save('pressure_bias.mat','pressure_bias');

figure
subplot(2,1,1)
plot(time,data(:,3)-pressure_bias(1),'r-',...
    time,data(:,5)-pressure_bias(2),'g-', ...
    time,data(:,7)-pressure_bias(3),'b-', ...
    time,data(:,9)-pressure_bias(4),'k-', ...
    time,data(:,11)-pressure_bias(5),'r--', ...
    time,data(:,13)-pressure_bias(6),'b--', ...
    time,data(:,15)-pressure_bias(7),'g--', ...
    time,data(:,17)-pressure_bias(8),'k--', ...
    time,data(:,19)-pressure_bias(9),'r:', ...
    time,data(:,21)-pressure_bias(10),'g:', ...
    time,data(:,23)-pressure_bias(11),'b:', ...
    time,data(:,25)-pressure_bias(12),'k:');
%legend('1','2','3','4','5','6','7','8','9','10','11','12');
title('DLHR');
xlabel('timestep')
ylabel('pressure [inH_2O]')
subplot(2,1,2)
plot(time,data(:,27),'r-');
title('DLV');
xlabel('timestep')
ylabel('pressure [psia]')

figure
subplot(2,1,1)
plot(time,data(:,4)-mean(data(1:20,4))+mean(data(1:20,4)),'r-',...
    time,data(:,6)-mean(data(1:20,6))+mean(data(1:20,4)),'g-', ...
    time,data(:,8)-mean(data(1:20,8))+mean(data(1:20,4)),'b-', ...
    time,data(:,10)-mean(data(1:20,10))+mean(data(1:20,4)),'k-', ...
    time,data(:,12)-mean(data(1:20,12))+mean(data(1:20,4)),'r--', ...
    time,data(:,14)-mean(data(1:20,14))+mean(data(1:20,4)),'b--', ...
    time,data(:,16)-mean(data(1:20,16))+mean(data(1:20,4)),'g--', ...
    time,data(:,18)-mean(data(1:20,18))+mean(data(1:20,4)),'k--', ...
    time,data(:,20)-mean(data(1:20,20))+mean(data(1:20,4)),'r:', ...
    time,data(:,22)-mean(data(1:20,22))+mean(data(1:20,4)),'g:', ...
    time,data(:,24)-mean(data(1:20,24))+mean(data(1:20,4)),'b:');
%legend('1','2','3','4','5','6','7','8','9','10','11');
title('DLHR');
xlabel('timestep')
ylabel('temperature [C]')
subplot(2,1,2)
plot(time,data(:,28)-data(1,28)+data(1,4),'r-');
title('DLV');
xlabel('timestep')
ylabel('temperature [C]')



